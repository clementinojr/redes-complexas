### B.1 Early Texture
1. First Order Statistics (FOS) / Statistical Features (SF)
2. Gray Level Co-occurence matrix (GLCM)
3. Gray Level Difference Statistics (GLDS)
4. Neighbourhood Gray Tone Difference Matrix (NGTDM)
5. Statistical Feature Matrix (SFM)
6. Laws Texture Energy (LTE/TEM)
7. Fractal Dimension Texture Analysis (FDTA)
8. Gray Level Run Length Matrix (GLRLM)
9. Fourier Power Spectrum (FPS)
10. Shape Parameters